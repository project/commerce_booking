<?php
/**
 * @file
 * Implementations of hooks in the commerce_booking group.
 */

/**
 * Implements hook_commerce_booking_ticket_price_alter().
 */
function commerce_booking_partial_tickets_commerce_booking_ticket_price_alter(&$price_field, $ticket, $options) {
  $instance = field_info_instance('cb_ticket_segment', 'cb_ticket_segment_price', 'cb_ticket_segment');

  // If we're using flexible pricing, pass onto the handler.
  if ($instance['widget']['type'] == 'commerce_booking_flexible_pricing_widget') {
    $options = isset($options['commerce_booking_flexible_pricing']) ? $options['commerce_booking_flexible_pricing']: array();
    $handler = CommerceFlexiblePricingHandlerTicketSegment::getHandler($ticket, $options);
    $price = $handler->calculatePrice($ticket);
  }
  // Otherwise, sum a simple price field.
  else {
    $wrapper = entity_metadata_wrapper('commerce_booking_ticket', $ticket);

    // Check that we have segments on this ticket.
    if (count($wrapper->cb_ticket_segments)) {
      $price = array(
        'amount' => 0,
        'currency_code' => commerce_default_currency(),
      );

      foreach ($wrapper->cb_ticket_segments as $segment) {
        if ($segment_price = $segment->cb_ticket_segment_price->value()) {
          $price['amount'] += commerce_currency_convert($segment_price['amount'], $segment_price['currency_code'], $price['currency_code']);
        }
      }
    }
  }

  // If we have a price, set it it the correct field structure.
  if (isset($price)) {
    $price_field = array(
      LANGUAGE_NONE => array($price),
    );
  }
}

/**
 * Implements hook_commerce_booking_flexible_pricing_fields_handlers().
 */
function commerce_booking_partial_tickets_commerce_booking_flexible_pricing_fields_handlers() {
  return array(
    'ticket_segment' => array(
      'handler' => 'CommerceFlexiblePricingHandlerTicketSegment',
      'label' => t('Ticket segments: Booking window/ticket class'),
    ),
  );
}
