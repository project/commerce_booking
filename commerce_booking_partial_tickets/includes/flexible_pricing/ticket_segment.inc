<?php
/**
 * @file
 * Contains the flexible pricing handler for duration based prices.
 */

/**
 * Ticket flexible pricing handler based on duration.
 */
class CommerceFlexiblePricingHandlerTicketSegment extends CommerceFlexiblePricingHandlerTicket {

  /**
   * Get a constructed price handler for ticket segments.
   *
   * @param $ticket
   *   The ticket we are working with.
   *
   * @return CommerceFlexiblePricingHandlerTicketSegment
   */
  public static function getHandler($ticket) {
    $field = field_info_field('cb_ticket_segment_price');
    $instance = field_info_instance('cb_ticket_segment', 'cb_ticket_segment_price', 'cb_ticket_segment');

    return new CommerceFlexiblePricingHandlerTicketSegment($field, $instance);
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePrice($entity, $options = array()) {
    // Get hold of all the segments on this ticket.
    $wrapper = entity_metadata_wrapper('commerce_booking_ticket', $entity);
    $segments = $wrapper->cb_ticket_segments->value();

    // If we have no segments, return NULL.
    if (count($segments) == 0) {
      return NULL;
    }

    // Set up the price field we'll add to.
    $price = array(
      'amount' => 0,
      'currency_code' => commerce_default_currency(),
    );

    // Get the x/y values if we are dealing with flexible pricing.
    $x_value = $this->getXValue($entity, $options);
    $y_value = $this->getYValue($entity, $options);

    // Loop over the segments.
    foreach ($segments as $segment) {
      // Retrieve the price items and map.
      $price_items = $this->getPriceItems($segment);

      // If we're using flexible pricing, get our map and find the right delta.
      $map = $this->buildPriceMap($price_items);

      // If the price exists, add it in.
      if (isset($map[$x_value][$y_value])) {
        $delta = $map[$x_value][$y_value];
        // If there is a price for this delta, add it in.
        if (isset($price_items[$delta])) {
          $price['amount'] += commerce_currency_convert($price_items[$delta]['amount'], $price_items[$delta]['currency_code'], $price['currency_code']);
        }
      }

    }

    return $price;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBookingWindows($entity, $sort = TRUE) {
    $event = $this->getEventEntity($entity);
    $booking_field = commerce_booking_field_get_field_name($event->type(), $event->value());
    $booking_instance = field_info_instance($event->type(), $booking_field, $event->getBundle());
    $price_field = $booking_instance['settings']['price_field'];
    $price_instance = field_info_instance($event->type(), $price_field, $event->getBundle());

    $field_name = isset($price_instance['widget']['settings']['flexible_pricing']['booking_window_field']) ? $price_instance['widget']['settings']['flexible_pricing']['booking_window_field']: NULL;
    if (empty($field_name)) { return array(); }
    $items = field_get_items($event->type(), $event->value(), $field_name);

    if ($sort && is_array($items)) {
      usort($items, array($this, 'sortBookingWindows'));
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTicketClasses($entity, $sort = TRUE) {
    $wrapper = $this->getEventEntity($entity);
    return commerce_booking_flexible_pricing_get_ticket_classes($wrapper, TRUE);
  }

  /**
   * Helper to track from a segment to an event.
   *
   * @param
   */
  protected function getEventEntity($segment) {
    $wrapper = entity_metadata_wrapper('cb_ticket_segment', $segment);
    return $wrapper->commerce_booking_event;
  }

  /**
   * {@inheritdoc}
   *
   * We create a spoof segment so everything else can work consistently.
   */
  protected function getPriceEntity($entity) {
    $segment = entity_create('cb_ticket_segment', array());
    $segment->commerce_booking_event = $entity->commerce_booking_event;
    return $segment;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldWidgetSettings() {
    return array();
  }

}
