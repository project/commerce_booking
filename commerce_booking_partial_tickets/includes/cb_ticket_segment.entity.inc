<?php

/**
 * @file
 * Provides the class for event segment entities.
 */

/**
 * Entity class for event segments.
 */
class CbTicketSegment extends Entity {

  /**
   * The label of the segment.
   *
   * @var string
   */
  public $label;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'cb_ticket_segment');
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    // We sit under the event, so get hold of that first.
    $wrapper = entity_metadata_wrapper($this->entityType, $this);
    $uri = entity_uri($wrapper->commerce_booking_event->type(), $wrapper->commerce_booking_event->value());

    // If it exists, add our additional path.
    if ($uri) {
      $uri['path'] .= '/segments/' . $this->id;
    }

    return $uri;
  }
}
