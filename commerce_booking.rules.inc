<?php
/**
 * @file
 * Rules integration for commerce_booking.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_booking_rules_event_info() {
  $events = array();
  $events['commerce_booking_ticket_confirm'] = array(
    'label' => t('When a ticket is confirmed'),
    'group' => t('Commerce Booking'),
    'help' => t('Runs whenever a ticket is confirmed for the first time.'),
    'variables' => array(
      'ticket' => array(
        'label' => t('Ticket'),
        'type' => 'commerce_booking_ticket',
        'skip save' => TRUE,
      ),
      'original_status' => array(
        'label' => t('Original Status'),
        'description' => t('The status of the ticket before it was confirmed.'),
        'type' => 'text',
        'options list' => commerce_booking_ticket_status_info(), 
      ),
      'store' => array(
        'label' => t('Store Flag'),
        'description' => t('Whether or not this ticket is going to be automatically saved after the confirmation.'),
        'type' => 'boolean',
      ),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 */
function commerce_booking_rules_condition_info() {
  return array(
    'commerce_booking_condition_entity_booking_open' => array(
      'label' => t('Entity is open for booking'),
      'parameter' => array(
        'entity' => array(
          'label' => t('Entity'),
          'type' => 'entity',
          'wrapped' => TRUE,
        ),
      ),
      'group' => t('Commerce Booking'),
    ),
    'commerce_booking_condition_same_entity' => array(
      'label' => t('Entities are the same'),
      'parameter' => array(
        'data' => array(
          'label' => t('Entity to compare'),
          'type' => 'entity',
          'wrapped' => TRUE,
        ),
        'value' => array(
          'label' => t('Entity to compare with'),
          'type' => 'entity',
          'wrapped' => TRUE,
        ),
      ),
      'group' => t('Data'),
    ),
    'commerce_booking_condition_ticket_is_booking_manager' => array(
      'label' => t('Ticket belongs to the Booking Manager'),
      'parameter' => array(
        'ticket' => array(
          'label' => t('Ticket'),
          'type' => 'entity',
          'wrapped' => TRUE,
        ),
      ),
      'group' => t('Commerce Booking'),
    ),
  );
}

/**
 * Rules condition for checking whether an entity is open for booking.
 *
 * @param EntityMetadataWrapper $entity_wrapper
 *   An entity metadata wrapper of the entity we're checking.
 *
 * @return bool
 */
function commerce_booking_condition_entity_booking_open($entity_wrapper) {
  $booking_field = commerce_booking_field_get_field_name($entity_wrapper->type(), $entity_wrapper->value());
  return isset($entity_wrapper->{$booking_field}) && $entity_wrapper->{$booking_field}->booking_open->value() == COMMERCE_BOOKING_BOOKING_OPEN;
}

/**
 * Rules condition for checking whether two entities are the same.
 *
 * @param EntityDrupalWrapper $data
 *   An entity metadata wrapper of the entity comparing.
 * @param EntityDrupalWrapper $value
 *   An entity metadata wrapper of the entity comparing with.
 *
 * @return bool
 */
function commerce_booking_condition_same_entity($data, $value) {
  return $data->type() == $value->type() && $data->getIdentifier() == $value->getIdentifier();
}

/**
 * Rules condition for checking whether a ticket belongs to the manager.
 */
function commerce_booking_condition_ticket_is_booking_manager($ticket) {
  return $ticket->party->raw() == $ticket->line_item->order->booking_manager->raw();
} 
