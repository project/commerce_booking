<?php
/**
 * @file
 * Contains commerce_booking_event_filter_options_list.
 */

/**
 * Class commerce_booking_event_filter_options_list.
 */
class commerce_booking_event_filter_options_list extends views_handler_filter_in_operator {

  /**
   * Overrides views_handler_filter_in_operator::option_definition().
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['entity_type'] = array('default' => '');
    $options['entity_id'] = array('default' => '');

    return $options;
  }

  /**
   * Overrides views_handler_filter_in_operator::expose_form().
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['entity_type'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Entity type'),
      '#description' => t('The type of entity we use for building our list of possible options. This can be retrieved from arguments using !0 etc.'),
      '#default_value' => !empty($this->options['entity_type']) ? $this->options['entity_type']: '',
    );
    $form['entity_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Event Entity ID'),
      '#description' => t('The id of the entity we use for building our list of possible options. This can be retrieved from arguments using !0 etc.'),
      '#default_value' => !empty($this->options['entity_id']) ? $this->options['entity_id']: '',
    );
  }

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (isset($this->value_options)) {
      return;
    }

    $this->value_options = array();

    $args = array();
    if (!empty($this->options['entity_type'])) {
      if (substr($this->options['entity_type'], 0, 1) == '!') {
        $args['type'] = @$this->view->args[substr($this->options['entity_type'], 1)];
      }
      else {
        $args['type'] = $this->options['entity_type'];
      }
    }

    if (!empty($this->options['entity_id'])) {
      if (substr($this->options['entity_id'], 0, 1) == '!') {
        $arg = substr($this->options['entity_id'], 1);
        if (!empty($this->view->args[$arg])) {
          $args['id'] = $this->view->args[$arg];
        }
      }
      else {
        $args['id'] = $this->options['entity_id'];
      }
    }

    if (count($args) == 2) {
      list($field_name, $entity_type, $bundle) = $this->definition['options arguments'];
      $field = field_info_field($field_name);
      $instance = field_info_instance($entity_type, $field_name, $bundle);
      /** @var EntityReference_SelectionHandler_CommerceBookingEvent $handler */
      $handler = entityreference_get_selection_handler($field, $instance);
      $handler->setEvent($args['id'], $args['type']);

      if ($options = $handler->getReferencableEntities()) {
        // Rebuild the array, by changing the bundle key into the bundle label.
        $target_type = $field['settings']['target_type'];
        $entity_info = entity_get_info($target_type);

        $return = array();
        foreach ($options as $bundle => $entity_ids) {
          $bundle_label = check_plain($entity_info['bundles'][$bundle]['label']);
          $return[$bundle_label] = $entity_ids;
        }

        $this->value_options = count($return) == 1 ? reset($return) : $return;
      }
    }

    return $this->value_options;
  }

}
