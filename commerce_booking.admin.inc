<?php
/**
 * @file
 * Admin pages for Commerce Booking.
 */

/**
 * Form API constructor for the Commerce Booking Configuration form.
 */
function commerce_booking_config_form($form, &$form_state) {
  $form['commerce_booking_unique_email'] = array(
    '#type' => 'select',
    '#title' => t('Unique email addresses'),
    '#options' => array(
      COMMERCE_BOOKING_UNIQUE_EMAIL_NONE => t('None'),
      COMMERCE_BOOKING_UNIQUE_EMAIL_BOOKING => t('Per booking'),
      COMMERCE_BOOKING_UNIQUE_EMAIL_EVENT => t('Per event'),
    ),
    '#description' => t('Choose the level at which to require unique email addresses for tickets. Cancelled tickets are ignored and the email may be left blank if not required. Note: this requires the field %field for the ticket type.', array('%field' => 'ticket_holder_email')),
    '#default_value' => variable_get('commerce_booking_unique_email', COMMERCE_BOOKING_UNIQUE_EMAIL_EVENT),
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return system_settings_form($form);
}
