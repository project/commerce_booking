<?php
/**
 * Administrative UIs for event segments.
 */

/**
 * Page callback for the add segment page.
 *
 * @param string $event_type
 *   The entity type of the event we're adding a segment for.
 * @param object $event
 *   The event object we are adding a segment for.
 *
 * @return array
 *  A render array.
 */
function commerce_booking_partial_tickets_segment_add($event_type, $event) {
  $segment = entity_create('cb_ticket_segment', array());
  $segment->commerce_booking_event[LANGUAGE_NONE][0] = array(
    'entity_type' => $event_type,
    'entity_id' => entity_id($event_type, $event),
  );

  // Pass onto the form constructor.
  return drupal_get_form('commerce_booking_partial_tickets_segment_form', $segment);
}

/**
 * Form API constructor for the add/edit event segment forms.
 *
 * @param Entity $segment
 *   The segment we are editing or NULL if we are adding.
 *
 * @return array
 *   The constructed form.
 *
 * @see commerce_booking_partial_tickets_segment_form_validate()
 * @see commerce_booking_partial_tickets_segment_form_submit()
 */
function commerce_booking_partial_tickets_segment_form($form, &$form_state, $segment) {
  // Get hold of the our segment/event wrappers.
  $wrapper = entity_metadata_wrapper('cb_ticket_segment', $segment);
  $form['#event'] = $wrapper->commerce_booking_event;
  $form['#entity'] = $segment;

  // Set the page title.
  if (!empty($segment->is_new)) {
    drupal_set_title(t('Add segment for @event', array(
      '@event' => $form['#event']->label(),
    )));
  }
  else {
    drupal_set_title(t('Edit @segment for @event', array(
      '@event' => $form['#event']->label(),
      '@segment' => $segment->label(),
    )));
  }

  // Set up our label.
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The label for this segment.'),
    '#default_value' => $segment->label(),
    '#required' => TRUE,
  );

  // Get any fields on the entity.
  field_attach_form('cb_ticket_segment', $segment, $form, $form_state);

  // Hide the event field.
  $form['commerce_booking_event']['#access'] = FALSE;

  // Add our actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($segment->is_new) ? t('Save segment') : t('Add segment'),
  );

  return $form;
}

/**
 * Validation handler for commerce_booking_partial_tickets_segment_form().
 *
 * @see commerce_booking_partial_tickets_segment_form()
 * @see commerce_booking_partial_tickets_segment_form_submit()
 */
function commerce_booking_partial_tickets_segment_form_validate(&$form, &$form_state) {
  // Pass on for field validation.
  field_attach_form_validate('cb_ticket_segment', $form['#entity'], $form, $form_state);
}

/**
 * Submission handler for commerce_booking_partial_tickets_segment_form().
 *
 * @see commerce_booking_partial_tickets_segment_form()
 * @see commerce_booking_partial_tickets_segment_form_validate()
 */
function commerce_booking_partial_tickets_segment_form_submit(&$form, &$form_state) {
  // Attach our values.
  $form['#entity']->label = $form_state['values']['label'];
  field_attach_submit('cb_ticket_segment', $form['#entity'], $form, $form_state);

  // Save our entity.
  $form['#entity']->save();

  // Redirect to the segment admin page.
  $form_state['redirect'] = entity_uri($form['#event']->type(), $form['#event']->value());
  $form_state['redirect']['path'] .= '/segments';
}

/**
 * Form constructor for the delete event segment form.
 *
 * @param Entity $segment
 *   The segment we are editing or NULL if we are adding.
 */
function commerce_booking_partial_tickets_segment_delete_form($form, &$form_state, $segment) {
  // Get hold of the our segment/event wrappers.
  $form['#segment'] = $segment;
  $wrapper = entity_metadata_wrapper('cb_ticket_segment', $segment);
  $form['#event'] = $wrapper->commerce_booking_event;

  // Set up our confirm_form() arguemnts.
  $question = t('Are you sure you want to delete %segment', array('%segment' => $segment->label()));
  $description = t('This will permanently delete %segment for %event', array(
    '%segment' => $segment->label(),
    '%event' => $form['#event']->label(),
  ));
  $uri = entity_uri($form['#event']->type(), $form['#event']->value());
  $uri['path'] .= '/segments';

  // Pass on to build the form.
  return confirm_form($form, $question, $uri, $description);
}

/**
 * Submission handler for commerce_booking_partial_tickets_segment_delete_form().
 *
 * @see commerce_booking_partial_tickets_segment_delete_form().
 */
function commerce_booking_partial_tickets_segment_delete_form_submit($form, &$form_state) {
  // Delete our entity.
  $form['#segment']->delete();

  // Redirect to the segment admin page.
  $form_state['redirect'] = entity_uri($form['#event']->type(), $form['#event']->value());
  $form_state['redirect']['path'] .= '/segments';
}
