<?php
/**
 * @file
 * Contains the flexible pricing handler for ticket class based prices.
 */

/**
 * Ticket flexible pricing handler based on booking window and ticket class.
 */
class CommerceFlexiblePricingHandlerTicket extends CommerceFlexiblePricingHandlerBookingWindowBase {

  /**
   * The key for the Y-axis.
   */
  protected $y_key = 'class';

  /**
   * Get a constructed price handler for a ticket.
   *
   * @param $ticket
   *   The ticket we are working with.
   *
   * @return CommerceFlexiblePricingHandlerTicket
   */
  public static function getHandler($ticket) {
    $ticket_wrapper = entity_metadata_wrapper('commerce_booking_ticket', $ticket);

    $booking_field_name = commerce_booking_field_get_field_name($ticket_wrapper->commerce_booking_event->type(), $ticket_wrapper->commerce_booking_event->value());
    $booking_field_instance = field_info_instance($ticket_wrapper->commerce_booking_event->type(), $booking_field_name, $ticket_wrapper->commerce_booking_event->getBundle());
    $price_field_name = $booking_field_instance['settings']['price_field'];

    $field = field_info_field($price_field_name);
    $instance = field_info_instance($ticket_wrapper->commerce_booking_event->type(), $price_field_name, $ticket_wrapper->commerce_booking_event->getBundle());

    return new CommerceFlexiblePricingHandlerTicket($field, $instance);
  }

  /**
   * Figure out the booking window (X-axis).
   *
   * @param $ticket
   *   The ticket we want the booking window for.
   * @param array $options
   *   Optionally provide additional options for the price calculation. Possible
   *   keys include:
   *     - date: The date to use for calculating the current booking window.
   *       Defaults to REQUEST_TIME.
   *
   * @return int
   *   The booking window id.
   */
  public function getXValue($ticket, $options = array()) {
    $ticket_wrapper = entity_metadata_wrapper('commerce_booking_ticket', $ticket);

    // Check to see if we have an override.
    if ($window = $ticket_wrapper->cbt_booking_window->value()) {
      return $window;
    }

    // Otherwise, use the normal process.
    return parent::getXValue($ticket, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function getYOptions($event) {
    $options = array();
    foreach ($this->getTicketClasses($event) as $class) {
      $options[$class->name] = $class->label;
    }
    return $options;
  }

  /**
   * Figure out the ticket class (Y-axis).
   *
   * @param $ticket
   *   The ticket we want the ticket class for.
   * @param array $options
   *   Optionally provide additional options for the price calculation. Possible
   *   keys include:
   *     - refresh_class: Whether to refresh the class cache. Defaults to FALSE.
   *
   * @return string
   *   A ticket class name.
   */
  public function getYValue($ticket, $options = array()) {
    $options += array('return' => 'name');
    $ticket_wrapper = entity_metadata_wrapper('commerce_booking_ticket', $ticket);

    // First look for a cached version if we are not refreshing.
    if (empty($options['refresh_class']) && $options['return'] != 'options') {
      if ($cached_class = $ticket_wrapper->cbt_class_cache->value()) {
        return $options['return'] == 'id' ? $cached_class->id : $cached_class->name;
      }
    }

    // See if there is a hard override.
    if ($class = $ticket_wrapper->commerce_booking_ticket_class->value()) {
      $ticket->cbt_class_cache[LANGUAGE_NONE][0]['target_id'] = $class->id;
      if ($options['return'] == 'options') {
        return array($class);
      }
      return $options['return'] == 'id' ? $class->id : $class->name;
    }

    // Otherwise, loop over the possible classes finding selected/first.
    $matches = array();
    $selected = $class = $ticket_wrapper->cbt_class_selection->value();
    foreach (commerce_booking_flexible_pricing_get_ticket_classes($ticket_wrapper->commerce_booking_event) as $class) {
      // Check the rule evaluates true!
      if (rules_invoke_component($class->name, $ticket)) {
        // If this was the first item and is not selectable, force this item.
        if (count($matches) == 0 && empty($class->settings['ticket_class_selectable'])) {
          $matches[] = $class;
          break;
        }

        // If selectable add to our matches.
        if (!empty($class->settings['ticket_class_selectable'])) {
          $matches[] = $class;
        }

        // If we aren't after all the options, we may be able to short-cut.
        // If there's no selection or this is our selection, we'll return it.
        if ($options['return'] != 'options' && (!$selected || $selected->id == $class->id)) {
          $matches = array(end($matches));
          break;
        }
      }
    }

    if ($options['return'] == 'options') {
      // If we only had one match and it's not selectable, return no options.
      return $matches;
    }
    // If we have a match, use it!
    elseif (!empty($matches)) {
      $match = reset($matches);
      $ticket->cbt_class_cache[LANGUAGE_NONE][0]['target_id'] = $match->id;
      return $options['return'] == 'id' ? $match->id : $match->name;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getPriceEntity($entity) {
    $wrapper = entity_metadata_wrapper('commerce_booking_ticket', $entity);
    return $wrapper->commerce_booking_event->value();
  }

  /**
   * Calculate the price for a ticket.
   *
   * @param $ticket
   *   The ticket we want the price for.
   * @param array $options
   *   Optionally provide additional options for the price calculation. Possible
   *   keys include:
   *     - date: The date to use for calculating the current booking window.
   *       Defaults to REQUEST_TIME.
   *     - refresh_class: Whether to refresh the class cache. Defaults to FALSE.
   *
   * @return array
   *   A commerce price field array or NULL if there is no valid value.
   */
  public function calculatePrice($ticket, $options = array()) {
    $price = parent::calculatePrice($ticket, $options);

    // If there is no price, return zero price.
    if ($price === NULL) {
      $price = array(
        'amount' => 0,
        'currency_code' => commerce_default_currency(),
      );
    }

    return $price;
  }

  /**
   * Get the ticket classes enabled on this event.
   *
   * @param object $event
   *   The event entity.
   *
   * @return array
   *   An array of ticket classes.
   */
  protected function getTicketClasses($event) {
    $wrapper = entity_metadata_wrapper($this->instance['entity_type'], $event);
    return commerce_booking_flexible_pricing_get_ticket_classes($wrapper);
  }

}
