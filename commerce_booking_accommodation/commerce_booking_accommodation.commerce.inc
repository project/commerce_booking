<?php
/**
 * @file
 * Contains Drupal Commerce hooks.
 */

/**
 * Implements hook_commerce_checkout_page_info().
 */
function commerce_booking_accommodation_commerce_checkout_page_info() {
  $pages = array();

  $pages['accommodation'] = array(
    'page_id' => 'accommodation',
    'title' => t('Accommodation'),
    'weight' => 3,
    'status_cart' => FALSE,
  );

  return $pages;
}

/**
 * Implements hook_commerce_checkout_pane_info().
 */
function commerce_booking_accommodation_commerce_checkout_pane_info() {
  $panes = array();

  $panes['accommodation'] = array(
    'pane_id' => 'accommodation',
    'title' => FALSE,
    'name' => t('Accommodation'),
    'page' => 'accommodation',
    'review' => FALSE,
    'file' => 'commerce_booking_accommodation.checkout.inc',
    'base' => 'commerce_booking_accommodation_checkout_pane_accommodation',
  );

  return $panes;
}

/**
 * Implements hook_commerce_checkout_pane_info_alter().
 */
function commerce_booking_accommodation_commerce_checkout_pane_info_alter(&$checkout_panes) {
  // If we are in the checkout process for a booking, hide the accommodation
  // step if the event doesn't have camping.
  if (arg(0) == 'checkout' && arg(1) && $order = entity_load_single('commerce_order', arg(1))) {
    $event = commerce_booking_get_event_entity($order);
    $event_type = commerce_booking_get_event_entity_type($order);
    $booking_field = commerce_booking_field_get_field_name($event_type, $event);
    if (empty($event->{$booking_field}[LANGUAGE_NONE][0]['settings']['accommodation'])) {
      unset($checkout_panes['accommodation']);
    }
  }
}

/**
 * Implements hook_commerce_line_item_type_info().
 */
function commerce_booking_accommodation_commerce_line_item_type_info() {
  $line_item_types = array();

  $line_item_types['commerce_booking_accommodation'] = array(
    'type' => 'commerce_booking_accommodation',
    'name' => t('Accommodation'),
    'description' => t('References an accommodation type.'),
    'product' => TRUE,
    'add_form_submit_value' => t('Add accommodation'),
    'base' => 'commerce_booking_accommodation_line_item_accommodation',
  );

  return $line_item_types;
}

/**
 * Title callback for commerce_booking_accommodation line item.
 */
function commerce_booking_accommodation_line_item_accommodation_title($line_item) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  return $wrapper->cba_accommodation_type->label();
}

/**
 * Form constuctor for adding a custom charge line item.
 */
function commerce_booking_accommodation_line_item_accommodation_add_form($element, &$form_state) {
  form_load_include($form_state, 'inc', 'commerce_booking_accommodation', 'commerce_booking_accommodation.commerce');
  $form = array();

  // Build a dummy line item.
  $values = array('type' => 'commerce_booking_accommodation');

  // If possible, get the order from the build info.
  if (isset($form_state['build_info']['args'][0]->order_id)) {
    $values['order_id'] = $form_state['build_info']['args'][0]->order_id;
  }

  $line_item = entity_create('commerce_line_item', $values);

  $options = array(
    'field_name' => 'cba_accommodation_type',
  );
  field_attach_form('commerce_line_item', $line_item, $form, $form_state, NULL, $options);
  $form['cba_accommodation_type']['#weight'] = -2;

  $options = array(
    'field_name' => 'cba_price_override',
  );
  field_attach_form('commerce_line_item', $line_item, $form, $form_state, NULL, $options);
  $form['cba_price_override']['#weight'] = -1;

  return $form;
}

/**
 * Submission handler for custom charge line item add form.
 */
function commerce_booking_accommodation_line_item_accommodation_add_form_submit(&$line_item, $element, &$form_state, $form) {
  $errors = form_get_errors();
  if (!empty($errors)) {
    return t('Unable to add the line item.');
  }

  $line_item->quantity = 1;
  field_attach_submit('commerce_line_item', $line_item, $form, $form_state);
}

/**
 * Calculate the value of an accommodation line item.
 */
function commerce_booking_accommodation_line_item_accommodation_process(&$line_item) {
  if ($line_item->type != 'commerce_booking_accommodation') {
    drupal_set_message(t('Invalid line item type.'), 'error');
    return;
  }

  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  if (!$line_item_wrapper->cba_accommodation_type->value()) {
    drupal_set_message(t('No accommodation selected.'), 'error');
    return;
  }

  // Don't recalculate if cancelled or paid in full. Check paid in full value
  // than using the status to allow cancellations etc.
  $line_item_balance = commerce_booking_line_item_balance($line_item_wrapper->value());
  if (!empty($line_item->is_new) || ($line_item_balance['amount'] > 0 && $line_item_wrapper->commerce_line_item_status->value() != 'cancelled')) {
    // @TODO: Cope without flexible pricing and remove it as a dependency
    $handler = CommerceFlexiblePricingHandlerAccommodationType::getHandler($line_item_wrapper->cba_accommodation_type->value());
    $price = (isset($line_item->cba_price_override[LANGUAGE_NONE][0]['amount'])) ? $line_item->cba_price_override[LANGUAGE_NONE][0] : $handler->calculatePrice($line_item);
    $line_item_wrapper->commerce_unit_price =
      $line_item_wrapper->commerce_total = $price;

    if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
      // Add the base price to the components array.
      if (!commerce_price_component_load($line_item_wrapper->commerce_unit_price->value(), 'base_price')) {
        $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
          $line_item_wrapper->commerce_unit_price->value(),
          'base_price',
          $line_item_wrapper->commerce_unit_price->value(),
          TRUE
        );
      }
    }

    if (!is_null($line_item_wrapper->commerce_total->value())) {
      // Add the base price to the components array.
      if (!commerce_price_component_load($line_item_wrapper->commerce_total->value(), 'base_price')) {
        $line_item_wrapper->commerce_total->data = commerce_price_component_add(
          $line_item_wrapper->commerce_total->value(),
          'base_price',
          $line_item_wrapper->commerce_total->value(),
          TRUE
        );
      }
    }

    // Re-calculate the balance.
    $line_item_balance = commerce_booking_line_item_balance($line_item_wrapper->value());
  }

  $status = $line_item_wrapper->commerce_line_item_status->value();
  if ($status == 'cancelled') {
    return;
  }
  elseif ($status == 'pending') {
    $paid = commerce_booking_line_item_balance($line_item_wrapper->value(), 'paid');
    if ($paid['amount']) {
      $line_item_wrapper->commerce_line_item_status = 'deposit_paid';
    }
  }

  if ($status == 'paid_in_full' && $line_item_balance['amount'] > 0) {
    $line_item_wrapper->commerce_line_item_status = 'deposit_paid';
  }
  elseif ($line_item_balance['amount'] <= 0) {
    $line_item_wrapper->commerce_line_item_status = 'paid_in_full';
  }
}

/**
 * Implements hook_commerce_payment_transaction_insert().
 */
function commerce_booking_accommodation_commerce_payment_transaction_insert($transaction, $type = 'commerce_payment_transaction') {
  // Check that we are in the checkout process (IPNs may have the 'cb_checkout'
  // GET/POST variable set).
  if (arg(0) != 'checkout' && empty($_REQUEST['cb_checkout'])) {
    return;
  }

  $transaction_wrapper = entity_metadata_wrapper('commerce_payment_transaction', $transaction);

  $order = $transaction_wrapper->order->value();

  if (empty($order->data['ticket_amounts'])) {
    return;
  }

  foreach ($order->data['ticket_amounts'] as $key => $amount) {
    $key_parts = explode('_', $key);
    if ($key_parts[0] != 'accommodation') {
      continue;
    }

    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $key_parts[1]);

    if ($line_item_wrapper->value()) {
      $item = entity_create('field_collection_item', array(
        'field_name' => 'commerce_booking_ticket_payments',
      ));
      $item->setHostEntity('commerce_line_item', $line_item_wrapper->value());

      $item_wrapper = entity_metadata_wrapper('field_collection_item', $item);
      $item_wrapper->cbt_payment_payment = $transaction;
      $item_wrapper->cbt_payment_value = $amount;
      $item_wrapper->save();
      $line_item_wrapper->save();
    }
  }
}

/**
 * Implements hook_commerce_payment_transaction_update().
 */
function commerce_booking_accommodation_commerce_payment_transaction_deferred_update($transaction, $type = 'commerce_payment_transaction') {
  // If this payment has changed to success, ensure that any tickets it makes a
  // payment for are tracked as confirmed.
  if ($transaction->status != 'success') {
    return;
  }

  // Look for any payment tracking for this particular payment.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'field_collection_item');
  $query->entityCondition('bundle', 'commerce_booking_ticket_payments');
  $query->fieldCondition('cbt_payment_payment', 'target_id', $transaction->transaction_id);
  $result = $query->execute();

  if (empty($result['field_collection_item'])) {
    return;
  }

  // Loop over each item and process it.
  foreach (array_keys($result['field_collection_item']) as $item_id) {
    $item = entity_metadata_wrapper('field_collection_item', $item_id);

    // If the amount tracked is not greater than zero, we won't confirm.
    if ($item->cbt_payment_value->amount->value() <= 0) {
      continue;
    }

    // Check that the host entity is an accommodation line item.
    $host = $item->host_entity;
    if ($host->type() == 'commerce_line_item' && $host->getBundle() == 'commerce_booking_accommodation') {
      // Simply save the line item to process it.
      commerce_line_item_save($host->value());
    }
  }
}
