<?php
/**
 * @file
 * Administration pages for the Commerce Booking Flexible Pricing Module
 */

/**
 * Form API Constructor for the ticket class admin page.
 *
 * @see commerce_booking_flexible_pricing_manage_ticket_classes_form_submit()
 */
function commerce_booking_flexible_pricing_manage_ticket_classes_form($form, &$form_state, $base_path) {
  $ui = new TicketClassUIController();
  $options = array(
    'base path' => $base_path,
    'show plugin' => FALSE,
    'show execution op' => FALSE,
  );
  $conditions = array(
    'plugin' => array('or', 'and'),
    'active' => TRUE,
    'tags' => array('Ticket Class'),
  );

  $form['enabled'] = $ui->overviewTable($conditions, $options);
  $form['enabled']['#weight'] = 0;
  $form['enabled']['#caption'] = t('Enabled ticket classes');
  $form['enabled']['#empty'] = t('There are no enabled ticket classes.');
  $form['enabled']['#pre_render'] = array('commerce_booking_flexible_pricing_ticket_class_table_pre_render');

  foreach ($form['enabled']['#rows'] as $id => $row) {
    $config = $row['weight']['data']['#config'];
    $form['enabled'][$id] = array(
      '#tree' => TRUE,
      '#parents' => array('rules_config', $id),
    );
    $form['enabled'][$id]['name'] = array(
      '#type' => 'value',
      '#value' => $config->name,
    );
    $form['enabled'][$id]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 100,
      '#default_value' => $config->weight,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @title', array('@title' => $config->label)),
    );
  }

  $form['action'] = array('#type' => 'actions', '#weight' => 1);
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save order'),
  );

  $options['show weight'] = FALSE;
  $conditions['active'] = FALSE;
  $form['disabled'] = $ui->overviewTable($conditions, $options);
  $form['disabled']['#weight'] = 2;
  $form['disabled']['#caption'] = t('Disabled ticket classes');
  $form['disabled']['#empty'] = t('There are no disabled ticket classes.');

  return $form;
}

/**
 * Submission handler for
 * commerce_booking_flexible_pricing_manage_ticket_classes_form().
 */
function commerce_booking_flexible_pricing_manage_ticket_classes_form_submit(&$form, &$form_state) {
  foreach($form_state['values']['rules_config'] as $values) {
    $config = rules_config_load($values['name']);
    if ($config->weight != $values['weight']) {
      $config->weight = $values['weight'];
      $config->save();
    }
  }
}

/**
 * Returns HTML for a ticket class sortable table.
 */
function commerce_booking_flexible_pricing_ticket_class_table_pre_render($element) {
  foreach (element_children($element) as $row_id) {
    $element[$row_id]['weight']['#attributes']['class'] = array('ticket-class-weight');

    $element['#rows'][$row_id]['weight'] = drupal_render($element[$row_id]);

    $element['#rows'][$row_id] = array(
      'data' => $element['#rows'][$row_id],
      'class' => array('draggable'),
    );
  }

  $element['#attributes']['id'] = drupal_html_id('ticket-class-overview');
  drupal_add_tabledrag($element['#attributes']['id'], 'order', 'sibling', 'ticket-class-weight');

  return $element;
}

/**
 * Form API constructor for the add ticket class form.
 */
function commerce_booking_flexible_pricing_add_ticket_class_form($form, $form_state, $base_path, $mode = NULL) {
  form_load_include($form_state, 'inc', 'rules_admin', 'rules_admin');
  if (!isset($form_state['values']['plugin_name'])) {
    $form_state['values']['plugin_name'] = 'and';
  }
  $form = rules_admin_add_component($form, $form_state, $base_path);
  $form['#validate'][] = 'rules_admin_add_component_validate';
  $form['#submit'][] = 'commerce_booking_flexible_pricing_ticket_class_form_submit';
  $form['#submit'][] = 'rules_admin_add_component_submit';

  if (!isset($form['settings'])) {
    // Limit the options to condition sets.
    $form['plugin_name']['#options'] = array_intersect_key($form['plugin_name']['#options'], array('and' => TRUE, 'or' => TRUE));

    return $form;
  }

  // Make sure 'Ticket Class' exists as a tag.
  if (empty($form['settings']['tags'])) {
    $form['settings']['tags']['#type'] = 'value';
    $form['settings']['tags']['#value'] = 'Ticket Class';
  }
  else {
    $form['settings']['tags']['#default_value'] = 'Ticket Class';
  }

  // Add the parameter.
  if (!empty($form['settings']['vars'])) {
    $form['settings']['vars']['#access'] = FALSE;
    $var = &$form['settings']['vars']['items'][0];

    // Fill in the first row.
    $var['type']['#value'] = 'commerce_booking_ticket';
    $var['type']['#disabled'] = TRUE;
    $var['label']['#value'] = t('Ticket');
    $var['label']['#disabled'] = TRUE;
    $var['name']['#value'] = 'ticket';
    $var['name']['#disabled'] = TRUE;

    // Hide the rest.
    unset($form['settings']['vars']['items'][1]);
    unset($form['settings']['vars']['items'][2]);

    // Hide the more link.
    unset($form['settings']['vars']['more']);
  }

  // Add our selectable option.
  $form['settings']['ticket_class_selectable'] = array(
    '#type' => 'checkbox',
    '#title' => t('This class is selectable in the checkout process'),
    '#description' => t('If multiple selectable ticket classes are available for a given ticket, the user is presented with the selection as part of the checkout process.'),
  );

  return $form;
}
