<?php
/**
 * @file
 * Page callbacks for commerce booking.
 */

/**
 * Page callback for editing tickets.
 */
function commerce_booking_edit_ticket($js, $ticket = NULL, $order = NULL) {
  if (!$ticket && !$order) {
    return MENU_NOT_FOUND;
  }

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($ticket, $order),
      ),
    );
    $commands = ctools_modal_form_wrapper('commerce_booking_edit_ticket_form', $form_state);

    if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('commerce_booking_edit_ticket_form', $ticket, $order);
  }
}

/**
 * Form constructor for the edit ticket form.
 *
 * @param CommerceBookingTicket $ticket
 *   A commerce_booking_ticket if we are editing one.
 * @param stdClass $order
 *   A commerce_order if we are adding a ticket.
 *
 * @see commerce_booking_edit_ticket_form_validate()
 * @see commerce_booking_edit_ticket_form_submit()
 */
function commerce_booking_edit_ticket_form($form, &$form_state, $ticket, $order) {
  // Track whether we are in the checkout as there may be some special bits.
  $form_state['in_checkout'] = !empty($form_state['in_checkout']) || arg(0) == 'checkout';

  // If we have a ticket in the form state, use that so changes are preserved.
  if (isset($form_state['ticket'])) {
    $ticket = $form_state['ticket'];
  }

  // If we are adding a ticket, let's create it.
  if ($order && !$ticket) {
    $ticket = commerce_booking_ticket_create($order);
  }
  // If we are editing, let's pull through the order.
  elseif ($ticket && !$order) {
    if ($line_item = commerce_booking_get_line_item($ticket)) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      $order = $line_item_wrapper->order->value();
    }
  }

  // If we haven't managed to figure out both, we can't continue.
  if (!$ticket || !$order) {
    return drupal_not_found();
  }

  // Get a lock check.
  $confirmed = !empty($ticket->confirmed);
  $locked = commerce_booking_ticket_is_locked($ticket);
  if ($locked) {
    drupal_set_message(t('This ticket is locked from editing. Changes made will not be saved.'), 'warning');
  }

  $form_state['build_info']['args'][0] = $form_state['ticket'] = $ticket;
  $form['#order']
    = $form_state['order'] = $order;

  // Attach the ticket form.
  field_attach_form('commerce_booking_ticket', $ticket, $form, $form_state);

  // Add our submit handlers.
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 99,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => !empty($ticket->is_new) ? t('Add Ticket') : t('Update ticket'),
    '#access' => !$locked,
  );
  $form['actions']['submit_confirm'] = array(
    '#type' => 'submit',
    '#value' => !empty($ticket->is_new) ? t('Add & Confirm Ticket') : t('Update & Confirm ticket'),
    '#access' => !$locked && !$confirmed && user_access('administer tickets'),
    '#ticket_action' => 'confirm',
  );
  $form['actions']['submit_unconfirm'] = array(
    '#type' => 'submit',
    '#value' => t('Update & Unconfirm ticket'),
    '#access' => !$locked && $confirmed && empty($ticket->is_new) && user_access('administer tickets'),
    '#ticket_action' => 'unconfirm',
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'op',
    '#limit_validation_errors' => array(),
    '#validate' => array(),
    '#submit' => array(),
  );
  return $form;
}

/**
 * Form validation handler for commerce_booking_edit_ticket_form().
 *
 * @see commerce_booking_edit_ticket_form_submit()
 */
function commerce_booking_edit_ticket_form_validate(&$form, &$form_state) {
  field_attach_form_validate('commerce_booking_ticket', clone $form_state['ticket'], $form, $form_state);

  $email_validation = variable_get('commerce_booking_unique_email', COMMERCE_BOOKING_UNIQUE_EMAIL_EVENT);
  if ($email_validation == COMMERCE_BOOKING_UNIQUE_EMAIL_NONE) {
    return;
  }

  // Check no other tickets with this email exist on this booking.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $form['#order']);

  if (!empty($form_state['values']['ticket_holder_email'][LANGUAGE_NONE][0]['email'])) {
    $email = $form_state['values']['ticket_holder_email'][LANGUAGE_NONE][0]['email'];

    // Perform email validation depending on the level.
    if ($email_validation == COMMERCE_BOOKING_UNIQUE_EMAIL_EVENT) {
      $event = $form_state['ticket']->wrapper()->commerce_booking_event;
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'commerce_booking_ticket');
      $query->fieldCondition('ticket_holder_email', 'email', $email);
      $query->fieldCondition('commerce_booking_event', 'entity_type', $event->type());
      $query->fieldCondition('commerce_booking_event', 'entity_id', $event->getIdentifier());
      $query->propertyCondition('status', 'cancelled', '!=');
      // Exclude this ticket, if it's been saved.
      if ($form_state['ticket']->ticket_id) {
        $query->entityCondition('entity_id', $form_state['ticket']->ticket_id, '!=');
      }
      $query->count();
      if ($query->execute()) {
        form_set_error('ticket_holder_email', t('A ticket with this email has already been booked onto %event. If you would like to continue with this ticket please change or remove the email.', array(
          '%event' => $event->label(),
        )));
      }
    }
    elseif ($email_validation == COMMERCE_BOOKING_UNIQUE_EMAIL_BOOKING) {
      // Iterate over each line item.
      foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
        // Check we are a relevant line item.
        if ($line_item_wrapper->getBundle() == 'commerce_booking_ticket') {
          // Iterate over each ticket.
          foreach ($line_item_wrapper->commerce_booking_ticket as $ticket_wrapper) {
            // If this ticket doesn't have an email field, skip it.
            if (!isset($ticket_wrapper->ticket_holder_email)) {
              continue;
            }

            // Check that this is a different ticket.
            if ($ticket_wrapper->getIdentifier() == $form_state['ticket']->ticket_id) {
              continue;
            }

            // Check the ticket isn't cancelled.
            if ($ticket_wrapper->status->value() == 'cancelled') {
              continue;
            }

            // Check the email is different.
            if ($ticket_wrapper->ticket_holder_email->value() == $email) {
              form_set_error('ticket_holder_email', t('A ticket with this email has already been added to your booking. If you would like to continue with this ticket please change or remove the email.'));
              // No need to check further.
              break;
            }
          }
        }
      }
    }
  }
}

/**
 * Form submission handler for commerce_booking_edit_ticket_form().
 *
 * @see commerce_booking_edit_ticket_form_validate()
 */
function commerce_booking_edit_ticket_form_submit(&$form, &$form_state) {
  $is_new = empty($form_state['ticket']->ticket_id);
  $order_wrapper = entity_metadata_wrapper("commerce_order", $form['#order']);

  field_attach_submit('commerce_booking_ticket', $form_state['ticket'], $form, $form_state);
  if (isset($form_state['triggering_element']['#ticket_action'])) {
    if (method_exists($form_state['ticket'], $form_state['triggering_element']['#ticket_action'])) {
      $form_state['ticket']->{$form_state['triggering_element']['#ticket_action']}(FALSE);
    }
  }
  entity_save('commerce_booking_ticket', $form_state['ticket']);

  if ($is_new) {
    module_load_include('inc', 'commerce_booking', 'commerce_booking.commerce');

    foreach ($order_wrapper->commerce_line_items as $wrapper) {
      if ($wrapper->getBundle() == 'commerce_booking_ticket') {
        $line_item_wrapper = $wrapper;
        break;
      }
    }

    if (!isset($line_item_wrapper)) {
      $line_item = commerce_line_item_new('commerce_booking_ticket', $order_wrapper->getIdentifier());
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      $line_item_wrapper->save();

      // Add the line item to the booking.
      $order_wrapper->commerce_line_items[] = $line_item;
    }

    $line_item_wrapper->commerce_booking_ticket[] = $form_state['ticket'];
    commerce_booking_ticket_process($form_state['ticket']);
    $line_item = $line_item_wrapper->value();
    commerce_booking_line_item_ticket_process($line_item);
    $line_item_wrapper->save();
  }

  // Make sure the order gets updated.
  $order_wrapper->save();
}

/**
 * Form submission handler for commerce_booking_edit_ticket_form().
 *
 * @see commerce_booking_edit_ticket_form_validate()
 */
function commerce_booking_edit_ticket_form_submit_confirm($form, &$form_state) {
  $form_state['ticket']->confirm();
}

/**
 * Form submission handler for commerce_booking_edit_ticket_form().
 *
 * @see commerce_booking_edit_ticket_form_validate()
 */
function commerce_booking_edit_ticket_form_submit_unconfirm($form, &$form_state) {
  $form_state['ticket']->unconfirm();
}

/**
 * Page callback for cancelling and refunding tickets.
 */
function commerce_booking_cancel_ticket($js, $ticket = NULL) {
  if (!$ticket) {
    return MENU_NOT_FOUND;
  }

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($ticket),
      ),
    );
    $commands = ctools_modal_form_wrapper('commerce_booking_cancel_ticket_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('commerce_booking_cancel_ticket_form', $ticket);
  }
}

/**
 * Form constructor for the edit ticket form.
 *
 * @param CommerceBookingTicket $ticket
 *   A commerce_booking_ticket to cancel and refund.
 *
 * @see commerce_booking_cancel_refund_ticket_form_confirm()
 */
function commerce_booking_cancel_ticket_form($form, &$form_state, $ticket) {
  $form['#ticket'] = $ticket;

  $form['#attributes']['class'][] = 'confirmation';
  $form['description'] = array('#markup' => t('Are you sure you want to cancel this ticket?'));
  if (user_access('administer tickets')) {
    $form['description']['#markup'] .= ' ' . t('You will have to refund any payments seperately.');
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel Ticket'),
    '#submit' => array(
      'commerce_booking_cancel_ticket_form_confirm',
    ),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array(),
  );

  // By default, render the form using theme_confirm_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'confirm_form';
  }

  return $form;
}

/**
 * Form submission handler for commerce_booking_cancel_refund_ticket_form().
 */
function commerce_booking_cancel_ticket_form_confirm(&$form, &$form_state) {
  $ticket = $form['#ticket'];
  $ticket_wrapper = entity_metadata_wrapper('commerce_booking_ticket', $ticket);
  $ticket_wrapper->status = 'cancelled';
  $ticket_wrapper->save();

  // Save the order to update the total.
  if ($line_item = commerce_booking_get_line_item($ticket)) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $order = $line_item_wrapper->order->value();
  }
  if ($order) {
    commerce_order_save($order);
  }
}

/**
 * Page callback for confirming tickets.
 */
function commerce_booking_confirm_ticket($js, $ticket = NULL) {
  if (!$ticket) {
    return MENU_NOT_FOUND;
  }

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($ticket),
      ),
    );
    $commands = ctools_modal_form_wrapper('commerce_booking_confirm_ticket_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('commerce_booking_confirm_ticket_form', $ticket);
  }
}

/**
 * Form constructor for the confirm ticket form.
 *
 * @param CommerceBookingTicket $ticket
 *   A commerce_booking_ticket to confirm.
 *
 * @see commerce_booking_confirm_ticket_form_confirm()
 */
function commerce_booking_confirm_ticket_form($form, &$form_state, $ticket) {
  if ($line_item = commerce_booking_get_line_item($ticket)) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $order = $line_item_wrapper->order->value();
  }

  $form['#ticket'] = $ticket;
  $form['#order']
    = $form_state['order'] = $order;

  $form['#attributes']['class'][] = 'confirmation';
  $form['description'] = array('#markup' => t('Are you sure you want to confirm this ticket?'));

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm Ticket'),
    '#submit' => array(
      'commerce_booking_confirm_ticket_form_confirm',
    ),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array(),
  );
  // By default, render the form using theme_confirm_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'confirm_form';
  }

  return $form;
}

/**
 * Form submission handler for commerce_booking_confirm_ticket_form().
 */
function commerce_booking_confirm_ticket_form_confirm(&$form, &$form_state) {
  $ticket = $form['#ticket'];
  $ticket->confirm();
}

/**
 * Page callback for cancelling and refunding tickets.
 */
function commerce_booking_move_ticket($js, $ticket = NULL) {
  if (!$ticket) {
    return MENU_NOT_FOUND;
  }

  if ($js) {
    // This is in a modal, so let's do the modal magic.
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($ticket),
      ),
    );
    $commands = ctools_modal_form_wrapper('commerce_booking_move_ticket_form', $form_state);

    if (!empty($form_state['executed'])) {
      // The form has been executed, so let's redirect to the destination page.
      $commands = array();
      $commands[] = ajax_command_invoke('#refresh-ticket-form', 'click');
      $commands[] = ctools_modal_command_dismiss();
    }

    print ajax_render($commands);
    ajax_footer();
    return;
  }
  else {
    // This is just a page, so we don't need to worry.
    return drupal_get_form('commerce_booking_move_ticket_form', $ticket);
  }
}

/**
 * Form constructor for the edit ticket form.
 *
 * @param CommerceBookingTicket $ticket
 *   A commerce_booking_ticket to cancel and refund.
 *
 * @see commerce_booking_cancel_refund_ticket_form_confirm()
 */
function commerce_booking_move_ticket_form($form, &$form_state, $ticket) {
  if ($line_item = commerce_booking_get_line_item($ticket)) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $order = $line_item_wrapper->order->value();
  }

  $form['#ticket'] = $ticket;
  $form['#order']
    = $form_state['order'] = $order;

  $form['#attributes']['class'][] = 'confirmation';
  $form['booking'] = array(
    '#type' => 'textfield',
    '#title' => t('New Booking'),
    '#description' => t('Please enter the booking reference of the booking you want to move this ticket to.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Move'),
    '#submit' => array(
      'commerce_booking_move_ticket_form_move',
    ),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array(),
  );
  // By default, render the form using theme_confirm_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'confirm_form';
  }

  return $form;
}

/**
 * Form submission handler for commerce_booking_cancel_refund_ticket_form().
 */
function commerce_booking_move_ticket_form_move(&$form, &$form_state) {
  drupal_set_message('This would normally move the ticket to the booking you specified and done something to work out the finances.', 'status');

  // Do some stuff to sort out the payments.
}
