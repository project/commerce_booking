<?php
/**
 * @file Contains the UI controller for Ticket Classes.
 */

/**
 * Controller class for Ticket Classes.
 */
class TicketClassUIController extends RulesUIController {

  /**
   * {@inheritdoc}
   */
  public function config_menu($base_path) {
    $items = parent::config_menu($base_path);
    $items[$base_path . '/manage/%rules_config']['title arguments'][0] = 'Edit ticket class !label';
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function overviewTable($conditions = array(), $options = array()) {
    $options += array('show weight' => TRUE);
    $table = parent::overviewTable($conditions, $options);

    if (isset($table['#header'][4]['colspan'])) {
      $table['#header'][4]['colspan']--;
    }
    uasort($table['#rows'], 'element_sort');
    foreach ($table['#rows'] as &$row) {
      unset($row['#weight']);
    }

    if ($options['show weight']) {
      $table['#header'][] = t('Weight');
      if (isset($table['#header'][4]['colspan'])) {
        $table['#header'][4]['colspan']--;
      }
    }

    return $table;
  }

  /**
   * {@inheritdoc}
   */
  protected function overviewTableRow($conditions, $name, $config, $options) {
    $row = parent::overviewTableRow($conditions, $name, $config, $options);
    unset($row[0]['data']['description']['variables']);

    if ($options['show weight']) {
      $row['weight']['data'] = array('#config' => $config);
    }

    $row['#weight'] = $config->weight;

    return $row;
  }
}
